Rails.application.routes.draw do
  resources :states
  resources :worlds
  resources :users

  resources :worlds do
    resources :states
  end
end
