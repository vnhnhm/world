json.extract! world, :id, :name, :user_id, :created_at, :updated_at
json.url world_url(world, format: :json)
