json.extract! state, :id, :user_id, :world_id, :created_at, :updated_at
json.url state_url(state, format: :json)
